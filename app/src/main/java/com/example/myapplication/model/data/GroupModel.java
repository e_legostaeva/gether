package com.example.myapplication.model.data;

import android.graphics.drawable.Drawable;

import java.util.UUID;

public class GroupModel {
    private UUID uuid;
    private String name;
    private Integer amount;
    private String type;
    private Drawable image;
    private Boolean isSubscribed;

    public GroupModel(UUID uuid, String name, Integer amount, String type, Drawable image, Boolean isSubscribed) {
        this.uuid = uuid;
        this.name = name;
        this.amount = amount;
        this.type = type;
        this.image = image;
        this.isSubscribed = isSubscribed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Boolean getSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(Boolean subscribed) {
        isSubscribed = subscribed;
    }
}
