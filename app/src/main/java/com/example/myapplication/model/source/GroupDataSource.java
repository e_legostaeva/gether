package com.example.myapplication.model.source;

import com.example.myapplication.model.data.GroupModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GroupDataSource {

    private final Map<UUID, GroupModel> groups = new HashMap<>();

    public GroupDataSource() {
        GroupModel groupModel = new GroupModel(UUID.randomUUID(), "Gaming Fury", 16, "Видеоигры", null, true);
        GroupModel groupModel1 = new GroupModel(UUID.randomUUID(), "D&D 5e kobold fight club", 42, "Настольные игры", null, true);
        GroupModel groupModel2 = new GroupModel(UUID.randomUUID(), "Школа молодого бойца", 66, "Спорт", null, false);
        GroupModel groupModel3 = new GroupModel(UUID.randomUUID(), "IT-помощник", 16, "Информационные технологии", null, true);
        GroupModel groupModel4 = new GroupModel(UUID.randomUUID(), "Овощи и семья", 42, "Садоводство", null, false);
        GroupModel groupModel5 = new GroupModel(UUID.randomUUID(), "Готовим для королей", 66, "Кулинария", null, false);
        GroupModel groupModel6 = new GroupModel(UUID.randomUUID(), "Настолки Академ", 16, "Настольные игры", null, true);
        GroupModel groupModel7 = new GroupModel(UUID.randomUUID(), "Спорт и я", 42, "Спорт", null, false);

        groups.put(groupModel.getUuid(), groupModel);
        groups.put(groupModel1.getUuid(), groupModel1);
        groups.put(groupModel2.getUuid(), groupModel2);
        groups.put(groupModel3.getUuid(), groupModel3);
        groups.put(groupModel4.getUuid(), groupModel4);
        groups.put(groupModel5.getUuid(), groupModel5);
        groups.put(groupModel6.getUuid(), groupModel6);
        groups.put(groupModel7.getUuid(), groupModel7);
    }

    public Collection<GroupModel> getAll() {
        return groups.values();
    }

    public GroupModel save(GroupModel group) {
        groups.put(group.getUuid(), group);
        return group;
    }
}
