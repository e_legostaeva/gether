package com.example.myapplication.model.source;

import com.example.myapplication.model.data.InterestModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InterestDataSource {

    private final Map<UUID, InterestModel> interests = new HashMap<>();

    public InterestDataSource() {
        InterestModel interestModel = new InterestModel(UUID.randomUUID(), "Готовка");
        InterestModel interestModel1 = new InterestModel(UUID.randomUUID(), "Ночные игры");
        InterestModel interestModel2 = new InterestModel(UUID.randomUUID(), "Программирование");

        interests.put(interestModel.getUuid(), interestModel);
        interests.put(interestModel1.getUuid(), interestModel1);
        interests.put(interestModel2.getUuid(), interestModel2);
    }

    public Collection<InterestModel> getAll() {
        return interests.values();
    }

    public InterestModel save(InterestModel meet) {
        interests.put(meet.getUuid(), meet);
        return meet;
    }
}
