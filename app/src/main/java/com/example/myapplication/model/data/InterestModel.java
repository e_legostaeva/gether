package com.example.myapplication.model.data;

import java.util.UUID;

public class InterestModel {
    private UUID uuid;
    private String name;

    public InterestModel(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
