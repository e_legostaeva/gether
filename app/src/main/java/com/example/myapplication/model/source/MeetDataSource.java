package com.example.myapplication.model.source;

import com.example.myapplication.model.data.MeetModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MeetDataSource {

    private final Map<UUID, MeetModel> meets = new HashMap<>();

    public MeetDataSource() {
        MeetModel meetModel = new MeetModel(UUID.randomUUID(), "Тусим до посинения", "ул. Пирогова 4, Новосибирск", "Сегодня, 18:00", true, false);
        MeetModel meetModel1 = new MeetModel(UUID.randomUUID(), "Групповая игра на музыкальных инструментах", "ул. Пирогова 4, Новосибирск", "Завтра, 2:20", false, false);
        MeetModel meetModel2 = new MeetModel(UUID.randomUUID(), "Отдыхаем от суеты", "ул. Пирогова 4, Новосибирск", "Завтра, 20:30", true, false);
        MeetModel meetModel3 = new MeetModel(UUID.randomUUID(), "Строим хату", "ул. Пирогова 4, Новосибирск", "Завтра, 12:00", false, false);
        MeetModel meetModel4 = new MeetModel(UUID.randomUUID(), "Соревнование по лепке снеговиков", "ул. Пирогова 4, Новосибирск", "Завтра, 18:20", true, false);
        MeetModel meetModel5 = new MeetModel(UUID.randomUUID(), "Кушаем снег", "ул. Пирогова 4, Новосибирск", "Завтра, 4:20", false, false);
        MeetModel meetModel6 = new MeetModel(UUID.randomUUID(), "Сдача проекта", "ул. Пирогова 4, Новосибирск", "Сегодня, 16:00", false, true);

        meets.put(meetModel.getUuid(), meetModel);
        meets.put(meetModel1.getUuid(), meetModel1);
        meets.put(meetModel2.getUuid(), meetModel2);
        meets.put(meetModel3.getUuid(), meetModel3);
        meets.put(meetModel4.getUuid(), meetModel4);
        meets.put(meetModel5.getUuid(), meetModel5);
        meets.put(meetModel6.getUuid(), meetModel6);
    }

    public Collection<MeetModel> getAll() {
        return meets.values();
    }

    public MeetModel save(MeetModel meet) {
        meets.put(meet.getUuid(), meet);
        return meet;
    }
}
