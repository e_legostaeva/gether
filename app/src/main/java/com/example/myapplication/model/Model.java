package com.example.myapplication.model;

import com.example.myapplication.model.data.GroupModel;
import com.example.myapplication.model.data.InterestModel;
import com.example.myapplication.model.data.MeetModel;
import com.example.myapplication.model.data.ProfileModel;
import com.example.myapplication.model.source.GroupDataSource;
import com.example.myapplication.model.source.InterestDataSource;
import com.example.myapplication.model.source.MeetDataSource;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

public class Model {
    private static volatile Model instance;
    private static MeetDataSource meetDataSource;
    private static GroupDataSource groupDataSource;
    private static InterestDataSource interestDataSource;
    private static SubscribeTicket groups;
    private static SubscribeTicket meets;
    private static SubscribeTicket interests;
    private static SubscribeTicket profile;
    private static ProfileModel activeProfile;

    private Model() {
        meetDataSource = new MeetDataSource();
        groupDataSource = new GroupDataSource();
        interestDataSource = new InterestDataSource();
        activeProfile = new ProfileModel(UUID.randomUUID(), "Cherry Boy Hunter", "Новосибирск", null);
        groups = new SubscribeTicket();
        meets = new SubscribeTicket();
        interests = new SubscribeTicket();
        profile = new SubscribeTicket();
    }

    public enum Notification {GROUP, MEET, INTEREST, PROFILE}

    public static Model getInstance() {
        Model result = instance;
        if (result != null) {
            return result;
        }
        synchronized (Model.class) {
            if (instance == null) {
                instance = new Model();
            }
            return instance;
        }
    }

    public GroupModel add(GroupModel group) {
        GroupModel save = groupDataSource.save(group);
        groups.notifyObservers(Notification.GROUP);
        return save;
    }

    public MeetModel add(MeetModel meet) {
        MeetModel save = meetDataSource.save(meet);
        meets.notifyObservers(Notification.MEET);
        return save;
    }

    public MeetModel modify(MeetModel meet) {
        Collection<MeetModel> all = meetDataSource.getAll();
        MeetModel match = null;
        for (MeetModel curr : all) {
            if (curr.getUuid().equals(meet.getUuid())) {
                match = curr;
                break;
            }
        }
        if (match == null) {
            return null;
        }
        meets.notifyObservers(Notification.MEET);
        return meetDataSource.save(meet);
    }

    public Collection<MeetModel> getMeets() {
        return new ArrayDeque<>(meetDataSource.getAll());
    }

    public Collection<GroupModel> getGroups() {
        return new ArrayDeque<>(groupDataSource.getAll());
    }

    public Collection<InterestModel> getInterests() {
        return new ArrayDeque<>(interestDataSource.getAll());
    }

    public ProfileModel getProfile() {
        return activeProfile;
    }

    public void subscribeToGroups(Observer o) {
        groups.addObserver(o);
    }

    public void subscribeToMeets(Observer o) {
        meets.addObserver(o);
    }

    public void subscribeToInterests(Observer o) {
        interests.addObserver(o);
    }

    public void subscribeToProfile(Observer o) {
        profile.addObserver(o);
    }


    private static class SubscribeTicket extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            super.setChanged();
            super.notifyObservers(arg);
        }
    }
}
