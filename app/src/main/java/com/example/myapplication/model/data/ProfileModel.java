package com.example.myapplication.model.data;

import android.graphics.drawable.Drawable;

import java.util.UUID;

public class ProfileModel {
    private UUID uuid;
    private String name;
    private String location;
    private Drawable image;

    public ProfileModel(UUID uuid, String name, String location, Drawable image) {
        this.uuid = uuid;
        this.name = name;
        this.location = location;
        this.image = image;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
