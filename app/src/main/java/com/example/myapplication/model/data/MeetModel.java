package com.example.myapplication.model.data;

import java.util.UUID;

public class MeetModel {
    private UUID uuid;
    private String name;
    private String place;
    private String time;
    private Boolean isFavourite;
    private Boolean isSubscribed;

    public MeetModel(UUID uuid, String name, String place, String time, Boolean isFavourite, Boolean isSubscribed) {
        this.uuid = uuid;
        this.name = name;
        this.place = place;
        this.time = time;
        this.isFavourite = isFavourite;
        this.isSubscribed = isSubscribed;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public Boolean getSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(Boolean subscribed) {
        isSubscribed = subscribed;
    }
}
