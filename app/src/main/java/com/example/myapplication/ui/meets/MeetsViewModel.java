package com.example.myapplication.ui.meets;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Model;
import com.example.myapplication.model.data.MeetModel;

import java.util.Collection;
import java.util.UUID;

public class MeetsViewModel extends ViewModel {

    public MeetsViewModel() {
    }

    public void createMeet(MeetModel meetModel) {
        meetModel.setUuid(UUID.randomUUID());
        Model.getInstance().add(meetModel);
    }

    public void changeFavourite(UUID uuid) {
        Collection<MeetModel> meets = Model.getInstance().getMeets();
        MeetModel match = null;
        for (MeetModel meet : meets) {
            if (meet.getUuid().equals(uuid)) {
                match = meet;
                break;
            }
        }
        if (match != null) {
            match.setFavourite(!match.getFavourite());
            Model.getInstance().modify(match);
        }
    }

    public void changeSubscribe(UUID uuid) {
        Collection<MeetModel> meets = Model.getInstance().getMeets();
        MeetModel match = null;
        for (MeetModel meet : meets) {
            if (meet.getUuid().equals(uuid)) {
                match = meet;
                break;
            }
        }
        if (match != null) {
            match.setSubscribed(!match.getSubscribed());
            Model.getInstance().modify(match);
        }
    }
}