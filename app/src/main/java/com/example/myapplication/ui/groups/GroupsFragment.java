package com.example.myapplication.ui.groups;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArraySet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.R;
import com.example.myapplication.model.data.GroupModel;
import com.example.myapplication.ui.util.GroupLayout;

import java.util.Collection;


public class GroupsFragment extends Fragment {

    private GroupsViewModel groupsViewModel;
    private final ArraySet<View> activeGroups = new ArraySet<>();
    private LinearLayout groupLayout;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        groupsViewModel = new ViewModelProvider(this).get(GroupsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_groups, container, false);
        Button button = root.findViewById(R.id.addGroupButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupsViewModel.createGroup(new GroupModel(null, "Новая группа", 1, "Тест", null, true));
            }
        });
        groupLayout = root.findViewById(R.id.meet_linear_layout);
        SearchView searchView = groupLayout.findViewById(R.id.search_bar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                groupsViewModel.queryMeets(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                groupsViewModel.queryMeets(newText);
                return false;
            }
        });
        groupsViewModel.getGroups().observe(getViewLifecycleOwner(), new Observer<Collection<GroupModel>>() {
            @Override
            public void onChanged(@Nullable Collection<GroupModel> c) {
                setGroups(c);
            }
        });
        return root;
    }

    private void setGroups(Collection<GroupModel> c) {
        for (View activeInterest : activeGroups) {
            groupLayout.removeView(activeInterest);
        }
        activeGroups.clear();
        for (GroupModel s : c) {
            View view = makeGroupView(s);
            activeGroups.add(view);
            groupLayout.addView(view);
        }
    }

    private View makeGroupView(GroupModel s) {
        return GroupLayout.builder(getContext())
                .name(s.getName())
                .amount(s.getAmount())
                .type(s.getType())
                .image(s.getImage())
                .build();
    }
}