package com.example.myapplication.ui.meets;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Model;
import com.example.myapplication.model.data.MeetModel;

import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class SubscribeViewModel extends ViewModel implements Observer {

    private final MutableLiveData<Collection<MeetModel>> mSubscribed;

    public SubscribeViewModel() {
        mSubscribed = new MutableLiveData<>();
        processGroups(Model.getInstance().getMeets());
        Model.getInstance().subscribeToMeets(this);
    }

    private void processGroups(Collection<MeetModel> meets) {
        Iterator<MeetModel> iterator = meets.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().getSubscribed()) {
                iterator.remove();
            }
        }
        mSubscribed.setValue(meets);
    }

    public LiveData<Collection<MeetModel>> getSubscribed() {
        return mSubscribed;
    }

    @Override
    public void update(Observable o, Object arg) {
        processGroups(Model.getInstance().getMeets());
    }
}
