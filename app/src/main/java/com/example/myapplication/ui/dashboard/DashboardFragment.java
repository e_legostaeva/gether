package com.example.myapplication.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArraySet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.R;
import com.example.myapplication.model.data.MeetModel;
import com.example.myapplication.ui.util.MeetLayout;

import java.util.Collection;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private LinearLayout layout;
    private final ArraySet<View> activeMeets = new ArraySet<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        layout = root.findViewById(R.id.meets_linear_layout);
        SearchView searchView = layout.findViewById(R.id.search_bar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                dashboardViewModel.queryMeets(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dashboardViewModel.queryMeets(newText);
                return false;
            }
        });
        dashboardViewModel.getMeets().observe(getViewLifecycleOwner(), new Observer<Collection<MeetModel>>() {
            @Override
            public void onChanged(@Nullable Collection<MeetModel> s) {
                setMeets(s);
            }
        });

        return root;
    }

    private void setMeets(Collection<MeetModel> c) {
        for (View activeInterest : activeMeets) {
            layout.removeView(activeInterest);
        }
        activeMeets.clear();
        for (MeetModel s : c) {
            View view = makeMeetView(s);
            Button starButton = view.findViewById(R.id.star_button);
            starButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dashboardViewModel.changeFavourite(s.getUuid());
                }
            });
            activeMeets.add(view);
            layout.addView(view);
        }
    }

    private View makeMeetView(MeetModel s) {
        return MeetLayout.builder(getContext())
                .name(s.getName())
                .time(s.getTime())
                .place(s.getPlace())
                .favourite(s.getFavourite())
                .starButtonListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dashboardViewModel.changeFavourite(s.getUuid());
                    }
                })
                .moreButtonListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu pm = new PopupMenu(getContext(), v);
                        pm.getMenuInflater().inflate(R.menu.pm_menu_items, pm.getMenu());
                        if (s.getSubscribed()) {
                            pm.getMenu().getItem(0).setTitle("Отписаться");
                        }
                        pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.subscribe:
                                        dashboardViewModel.changeSubscribe(s.getUuid());
                                        break;
                                    case R.id.report:
                                        Toast.makeText(getContext(), "Жалоба была отправлена", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                                return true;
                            }
                        });
                        pm.show();
                    }
                })
                .build();
    }
}