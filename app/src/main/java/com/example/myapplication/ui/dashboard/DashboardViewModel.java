package com.example.myapplication.ui.dashboard;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Model;
import com.example.myapplication.model.data.MeetModel;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

public class DashboardViewModel extends ViewModel implements Observer {

    private MutableLiveData<List<MeetModel>> mMeets;
    private String lastQuery = "";

    public DashboardViewModel() {
        Model instance = Model.getInstance();
        mMeets = new MutableLiveData<>();
        instance.subscribeToMeets(this);
        setValue(instance.getMeets());
    }

    public void changeFavourite(UUID uuid) {
        Collection<MeetModel> meets = Model.getInstance().getMeets();
        MeetModel match = null;
        for (MeetModel meet : meets) {
            if (meet.getUuid().equals(uuid)) {
                match = meet;
                break;
            }
        }
        if (match != null) {
            match.setFavourite(!match.getFavourite());
            Model.getInstance().modify(match);
            setValue(Model.getInstance().getMeets());
        }
    }

    public MutableLiveData<List<MeetModel>> getMeets() {
        return mMeets;
    }

    public void queryMeets(String query) {
        lastQuery = query;
        Collection<MeetModel> copy = new ArrayDeque<>(Model.getInstance().getMeets());
        Iterator<MeetModel> iterator = copy.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().getName().toLowerCase(Locale.getDefault()).contains(lastQuery.toLowerCase())) {
                iterator.remove();
            }
        }
        setValue(copy);
    }

    private void setValue(Collection<MeetModel> models) {
        List<MeetModel> meetModels = new ArrayList<>(models);
        meetModels = _queryMeets(meetModels);
        Collections.sort(meetModels, (o1, o2) -> o1.getTime().compareToIgnoreCase(o2.getTime()));
        mMeets.setValue(meetModels);
    }

    private List<MeetModel> _queryMeets(Collection<MeetModel> meetModels) {
        List<MeetModel> copy = new ArrayList<>(meetModels);
        Iterator<MeetModel> iterator = copy.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().getName().toLowerCase(Locale.getDefault()).contains(lastQuery.toLowerCase())) {
                iterator.remove();
            }
        }
        return copy;
    }

    @Override
    public void update(Observable o, Object arg) {
        setValue(Model.getInstance().getMeets());
    }

    public void changeSubscribe(UUID uuid) {
        Collection<MeetModel> meets = Model.getInstance().getMeets();
        MeetModel match = null;
        for (MeetModel meet : meets) {
            if (meet.getUuid().equals(uuid)) {
                match = meet;
                break;
            }
        }
        if (match != null) {
            match.setSubscribed(!match.getSubscribed());
            Model.getInstance().modify(match);
        }
    }
}