package com.example.myapplication.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

public class EnterNumberActivity extends AppCompatActivity {

    private EnterNumberViewModel enterNumberViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_number);
        configureButtons();
        View sendButton = findViewById(R.id.sendVerificationButton);
        EditText phoneText = findViewById(R.id.editTextPhone);
        enterNumberViewModel = new ViewModelProvider(this).get(EnterNumberViewModel.class);
        enterNumberViewModel.getLoginFormState().observe(this, loginFormState -> {
            if (loginFormState == null) {
                return;
            }
            sendButton.setEnabled(loginFormState.isDataValid());
            if (!loginFormState.isDataValid() && loginFormState.getErrorId() != null) {
                phoneText.setError(getString(loginFormState.getErrorId()));
            }
        });


        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                enterNumberViewModel.loginDataChanged(phoneText.getText().toString());
            }
        };
        phoneText.addTextChangedListener(afterTextChangedListener);
    }

    private void configureButtons() {
        Button sendButton = findViewById(R.id.sendVerificationButton);
        sendButton.setOnClickListener(v -> {
            Intent intent = new Intent(EnterNumberActivity.this, NumberVerificationActivity.class);
            EditText editText = findViewById(R.id.editTextPhone);
            Editable text = editText.getText();
            intent.putExtra("PHONE_NUMBER", text.toString().trim());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        Button backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(v -> {
            startActivity(new Intent(EnterNumberActivity.this, LoginActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });

        Button googleButton = findViewById(R.id.loginGoogle);
        googleButton.setOnClickListener(v -> {
            startActivity(new Intent(EnterNumberActivity.this, MainActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });
    }
}