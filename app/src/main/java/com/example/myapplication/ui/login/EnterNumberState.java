package com.example.myapplication.ui.login;

import androidx.annotation.Nullable;

/**
 * Data validation state of the login form.
 */
class
EnterNumberState {
    @Nullable
    private final Integer errorId;
    private final boolean isDataValid;

    EnterNumberState(@Nullable Integer errorId) {
        this.errorId = errorId;
        this.isDataValid = false;
    }

    EnterNumberState(boolean isDataValid) {
        this.errorId = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getErrorId() {
        return errorId;
    }

    boolean isDataValid() {
        return isDataValid;
    }
}