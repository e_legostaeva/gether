package com.example.myapplication.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

public class NumberVerificationActivity extends AppCompatActivity {

    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_verification_attivity);
        phoneNumber = getIntent().getStringExtra("PHONE_NUMBER");
        configureText();
        configureButtons();
    }

    private void configureButtons() {
        Button backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(v -> {
            startActivity(new Intent(NumberVerificationActivity.this, LoginActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });

        Button verifyButton = findViewById(R.id.sendVerificationButton);
        verifyButton.setOnClickListener(v -> {
            startActivity(new Intent(NumberVerificationActivity.this, MainActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });
    }

    private void configureText() {
        TextView textView = findViewById(R.id.textViewInfoPhone);
        textView.setText(String.format(getString(R.string.text_pattern_verification_code), phoneNumber));
    }
}