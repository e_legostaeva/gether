package com.example.myapplication.ui.profile;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.R;
import com.example.myapplication.model.Model;
import com.example.myapplication.model.data.GroupModel;
import com.example.myapplication.model.data.InterestModel;
import com.example.myapplication.model.data.ProfileModel;

import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class ProfileViewModel extends AndroidViewModel implements Observer {

    private final MutableLiveData<String> mProfileName;
    private final MutableLiveData<String> mLocationString;
    private final MutableLiveData<Drawable> mProfileImage;
    private final MutableLiveData<Collection<InterestModel>> mInterests;
    private final MutableLiveData<Collection<GroupModel>> mGroups;

    public ProfileViewModel(Application app) {
        super(app);
        Model instance = Model.getInstance();
        mProfileName = new MutableLiveData<>();
        mLocationString = new MutableLiveData<>();
        mProfileImage = new MutableLiveData<>();
        mInterests = new MutableLiveData<>();
        mGroups = new MutableLiveData<>();

        processGroups(instance.getGroups());
        processInterests(instance.getInterests());
        processProfile(instance.getProfile());
        instance.subscribeToGroups(this);
        instance.subscribeToInterests(this);
        instance.subscribeToProfile(this);
    }

    private void processProfile(ProfileModel profile) {
        mProfileName.setValue(profile.getName());
        mLocationString.setValue(profile.getLocation());
        mProfileImage.setValue(ContextCompat.getDrawable(getApplication().getApplicationContext(), R.drawable.profile));
    }

    public LiveData<String> getProfileName() {
        return mProfileName;
    }

    public LiveData<String> getLocation() {
        return mLocationString;
    }

    public LiveData<Drawable> getProfileImage() {
        return mProfileImage;
    }

    public LiveData<Collection<InterestModel>> getInterests() {
        return mInterests;
    }

    public LiveData<Collection<GroupModel>> getGroups() {
        return mGroups;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals(Model.Notification.GROUP)) {
            processGroups(Model.getInstance().getGroups());
        } else if (arg.equals(Model.Notification.INTEREST)) {
            processInterests(Model.getInstance().getInterests());
        }
    }

    private void processGroups(Collection<GroupModel> groups) {
        Drawable drawable = ContextCompat.getDrawable(getApplication().getApplicationContext(), R.drawable.profile);
        Iterator<GroupModel> iterator = groups.iterator();
        while (iterator.hasNext()) {
            GroupModel next = iterator.next();
            if (!next.getSubscribed()) {
                iterator.remove();
            } else {
                next.setImage(drawable);
            }
        }
        mGroups.setValue(groups);
    }

    private void processInterests(Collection<InterestModel> interests) {
        mInterests.setValue(interests);
    }
}