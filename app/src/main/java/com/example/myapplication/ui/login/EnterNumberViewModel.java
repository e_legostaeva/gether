package com.example.myapplication.ui.login;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.R;

public class EnterNumberViewModel extends ViewModel {

    private final MutableLiveData<EnterNumberState> loginFormState = new MutableLiveData<>();

    public EnterNumberViewModel() {
        loginFormState.setValue(new EnterNumberState(false));
    }

    LiveData<EnterNumberState> getLoginFormState() {
        return loginFormState;
    }

    public void loginDataChanged(String phoneNumber) {
        if (!isUserNameValid(phoneNumber)) {
            loginFormState.setValue(new EnterNumberState(R.string.wrong_number_format));
        } else {
            loginFormState.setValue(new EnterNumberState(true));
        }
    }

    private boolean isUserNameValid(String phoneNumber) {
        if (phoneNumber == null) {
            return false;
        }
        return Patterns.PHONE.matcher(phoneNumber).matches();
    }
}