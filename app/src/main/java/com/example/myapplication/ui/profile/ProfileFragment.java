package com.example.myapplication.ui.profile;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArraySet;
import androidx.constraintlayout.helper.widget.Flow;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.R;
import com.example.myapplication.SettingsActivity;
import com.example.myapplication.model.data.GroupModel;
import com.example.myapplication.model.data.InterestModel;
import com.example.myapplication.ui.util.GroupLayout;

import java.util.Collection;


public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private ConstraintLayout layout;
    private Flow flow;
    private final ArraySet<View> activeInterests = new ArraySet<>();
    private LinearLayout groupLayout;
    private final ArraySet<View> activeGroups = new ArraySet<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        final TextView profileNameText = root.findViewById(R.id.profileName);
        final TextView locationText = root.findViewById(R.id.locationText);
        final Button settingsButton = root.findViewById(R.id.settingButton);
        ImageView imageView = root.findViewById(R.id.profileImage);

        profileViewModel.getProfileImage().observe(getViewLifecycleOwner(), new Observer<Drawable>() {
            @Override
            public void onChanged(@Nullable Drawable d) {
                imageView.setImageDrawable(d);
            }
        });
        profileViewModel.getProfileName().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                profileNameText.setText(s);
            }
        });

        profileViewModel.getLocation().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                locationText.setText(s);
            }
        });

        profileViewModel.getInterests().observe(getViewLifecycleOwner(), new Observer<Collection<InterestModel>>() {
            @Override
            public void onChanged(@Nullable Collection<InterestModel> c) {
                setInterests(c);
            }
        });

        profileViewModel.getGroups().observe(getViewLifecycleOwner(), new Observer<Collection<GroupModel>>() {
            @Override
            public void onChanged(@Nullable Collection<GroupModel> c) {
                setGroups(c);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SettingsActivity.class));
            }
        });

        layout = root.findViewById(R.id.constraint_layout);
        flow = root.findViewById(R.id.flowInterests);
        groupLayout = root.findViewById(R.id.groups_layout);
        return root;
    }

    private void setGroups(Collection<GroupModel> c) {
        for (View activeInterest : activeGroups) {
            groupLayout.removeView(activeInterest);
        }
        activeGroups.clear();
        for (GroupModel s : c) {
            View view = makeGroupView(s);
            activeGroups.add(view);
            groupLayout.addView(view);
        }
    }

    private View makeGroupView(GroupModel s) {
        return GroupLayout.builder(getContext())
                .name(s.getName())
                .amount(s.getAmount())
                .type(s.getType())
                .image(s.getImage())
                .build();
    }

    private void setInterests(Collection<InterestModel> c) {
        for (View activeInterest : activeInterests) {
            flow.removeView(activeInterest);
        }
        activeInterests.clear();
        for (InterestModel s : c) {
            View view = makeInterestView(s);
            activeInterests.add(view);
            layout.addView(view);
            flow.addView(view);
        }
    }

    //TODO: сделать красивыми и перетащить в фабрику, можно локальную
    private View makeInterestView(InterestModel text) {
        TextView tv = new TextView(getContext());
        tv.setId(View.generateViewId());
        tv.setText(text.getName());
        return tv;
    }
}