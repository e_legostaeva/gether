package com.example.myapplication.ui.groups;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.R;
import com.example.myapplication.model.Model;
import com.example.myapplication.model.data.GroupModel;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

public class GroupsViewModel extends AndroidViewModel implements Observer {

    private final MutableLiveData<List<GroupModel>> mGroups;

    public GroupsViewModel(Application app) {
        super(app);
        Model model = Model.getInstance();
        mGroups = new MutableLiveData<>();
        Drawable drawable = ContextCompat.getDrawable(getApplication().getApplicationContext(), R.drawable.profile);
        Collection<GroupModel> groups = model.getGroups();
        for (GroupModel group : groups) {
            group.setImage(drawable);
        }
        setValue(groups);
        model.subscribeToGroups(this);
    }

    public LiveData<List<GroupModel>> getGroups() {
        return mGroups;
    }

    private void setValue(Collection<GroupModel> groups) {
        //TODO: Добавить query проверку
        ArrayList<GroupModel> groupModels = new ArrayList<>(groups);
        Collections.sort(groupModels, (o1, o2) -> o1.getAmount().compareTo(o2.getAmount()));
        Collections.reverse(groupModels);
        mGroups.setValue(groupModels);
    }

    @Override
    public void update(Observable o, Object arg) {
        Collection<GroupModel> groups = Model.getInstance().getGroups();
        Drawable drawable = ContextCompat.getDrawable(getApplication().getApplicationContext(), R.drawable.profile);
        for (GroupModel group : groups) {
            group.setImage(drawable);
        }
        setValue(groups);
    }

    public void queryMeets(String query) {
        Collection<GroupModel> copy = new ArrayDeque<>(Model.getInstance().getGroups());
        Iterator<GroupModel> iterator = copy.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().getName().toLowerCase(Locale.getDefault()).contains(query.toLowerCase())) {
                iterator.remove();
            }
        }
        setValue(copy);
    }

    public void createGroup(GroupModel groupModel) {
        groupModel.setUuid(UUID.randomUUID());
        Model.getInstance().add(groupModel);
    }
}