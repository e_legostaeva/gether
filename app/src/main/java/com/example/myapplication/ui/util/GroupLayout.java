package com.example.myapplication.ui.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.myapplication.R;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public class GroupLayout {
    public static class GroupLayoutBuilder {
        private final Context context;
        private String name;
        private String type;
        private Integer amount;
        private Drawable image;

        public GroupLayoutBuilder(Context context) {
            this.context = context;
        }

        public GroupLayoutBuilder name(String name) {
            this.name = name;
            return this;
        }

        public GroupLayoutBuilder type(String type) {
            this.type = type;
            return this;
        }

        public GroupLayoutBuilder amount(Integer amount) {
            this.amount = amount;
            return this;
        }

        public GroupLayoutBuilder image(Drawable image) {
            this.image = image;
            return this;
        }

        public ConstraintLayout build() {
            ConstraintLayout layout = (ConstraintLayout) LayoutInflater.from(context).inflate(R.layout.group_layout, null);
            TextView name = layout.findViewById(R.id.group_name);
            ImageView picture = layout.findViewById(R.id.group_image);
            TextView type = layout.findViewById(R.id.group_type);
            TextView amount = layout.findViewById(R.id.group_amount);
            name.setText(this.name);
            picture.setImageDrawable(this.image);
            type.setText(this.type);
            //TODO: сделать нормальный перевод в строку
            amount.setText(String.format(Locale.getDefault(), "%d участника", this.amount));
            return layout;
        }

    }

    public static GroupLayoutBuilder builder(@NotNull Context context) {
        return new GroupLayoutBuilder(context);
    }
}
