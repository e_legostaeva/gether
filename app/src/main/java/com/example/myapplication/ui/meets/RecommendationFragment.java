package com.example.myapplication.ui.meets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.R;
import com.example.myapplication.model.data.MeetModel;

import java.util.UUID;

public class RecommendationFragment extends Fragment {

    private MeetsViewModel meetsViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        meetsViewModel = new ViewModelProvider(this).get(MeetsViewModel.class);
        View inflate = inflater.inflate(R.layout.fragment_recommendation, container, false);
        Button button = inflate.findViewById(R.id.addGroupButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meetsViewModel.createMeet(new MeetModel(UUID.randomUUID(), "Новое мероприятие", "Где-то", "Когда-то", false, true));
            }
        });
        return inflate;
    }
}