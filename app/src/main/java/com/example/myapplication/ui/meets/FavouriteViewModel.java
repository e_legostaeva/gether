package com.example.myapplication.ui.meets;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Model;
import com.example.myapplication.model.data.MeetModel;

import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class FavouriteViewModel extends ViewModel implements Observer {

    private final MutableLiveData<Collection<MeetModel>> mFavourites;

    public FavouriteViewModel() {
        mFavourites = new MutableLiveData<>();
        processGroups(Model.getInstance().getMeets());
        Model.getInstance().subscribeToMeets(this);
    }

    private void processGroups(Collection<MeetModel> meets) {
        Iterator<MeetModel> iterator = meets.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().getFavourite()) {
                iterator.remove();
            }
        }
        mFavourites.setValue(meets);
    }

    public LiveData<Collection<MeetModel>> getFavourites() {
        return mFavourites;
    }

    @Override
    public void update(Observable o, Object arg) {
        processGroups(Model.getInstance().getMeets());
    }
}