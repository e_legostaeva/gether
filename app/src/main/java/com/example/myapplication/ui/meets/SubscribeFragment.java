package com.example.myapplication.ui.meets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.collection.ArraySet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.R;
import com.example.myapplication.model.data.MeetModel;
import com.example.myapplication.ui.util.MeetLayout;

import java.util.Collection;
import java.util.UUID;

public class SubscribeFragment extends Fragment {

    private MeetsViewModel meetsViewModel;
    private SubscribeViewModel subscribeViewModel;
    private final ArraySet<View> activeMeets = new ArraySet<>();
    private LinearLayout layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        meetsViewModel = new ViewModelProvider(this).get(MeetsViewModel.class);
        subscribeViewModel = new ViewModelProvider(this).get(SubscribeViewModel.class);
        subscribeViewModel.getSubscribed().observe(getViewLifecycleOwner(), new Observer<Collection<MeetModel>>() {
            @Override
            public void onChanged(Collection<MeetModel> meetModels) {
                setMeets(meetModels);
            }
        });
        View inflate = inflater.inflate(R.layout.fragment_subscribe, container, false);
        layout = inflate.findViewById(R.id.meet_linear_layout);
        Button button = inflate.findViewById(R.id.addGroupButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meetsViewModel.createMeet(new MeetModel(UUID.randomUUID(), "Новое мероприятие", "Где-то", "Когда-то", false, true));
            }
        });
        return inflate;
    }

    private void setMeets(Collection<MeetModel> meetModels) {
        for (View activeInterest : activeMeets) {
            layout.removeView(activeInterest);
        }
        activeMeets.clear();
        for (MeetModel s : meetModels) {
            View view = makeMeetView(s);
            activeMeets.add(view);
            layout.addView(view);
        }
    }

    private View makeMeetView(MeetModel s) {
        return MeetLayout.builder(getContext())
                .name(s.getName())
                .time(s.getTime())
                .place(s.getPlace())
                .favourite(s.getFavourite())
                .starButtonListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        meetsViewModel.changeFavourite(s.getUuid());
                    }
                })
                .moreButtonListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu pm = new PopupMenu(getContext(), v);
                        pm.getMenuInflater().inflate(R.menu.pm_menu_items, pm.getMenu());
                        if (s.getSubscribed()) {
                            pm.getMenu().getItem(0).setTitle("Отписаться");
                        }
                        pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.subscribe:
                                        meetsViewModel.changeSubscribe(s.getUuid());
                                        break;
                                    case R.id.report:
                                        Toast.makeText(getContext(), "Жалоба была отправлена", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                                return true;
                            }
                        });
                        pm.show();
                    }
                })
                .build();
    }
}