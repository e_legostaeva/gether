package com.example.myapplication.ui.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.example.myapplication.R;
import com.google.android.material.card.MaterialCardView;

public class MeetLayout {
    public static class MeetLayoutBuilder {
        private final Context context;
        private String name;
        private String place;
        private String time;
        private Boolean isFavourite;
        private View.OnClickListener moreListener;
        private View.OnClickListener starListener;

        public MeetLayoutBuilder(Context context) {
            this.context = context;
        }

        public MeetLayoutBuilder name(String name) {
            this.name = name;
            return this;
        }

        public MeetLayoutBuilder time(String time) {
            this.time = time;
            return this;
        }

        public MeetLayoutBuilder place(String place) {
            this.place = place;
            return this;
        }

        public MeetLayoutBuilder favourite(Boolean b) {
            this.isFavourite = b;
            return this;
        }

        public MeetLayoutBuilder moreButtonListener(View.OnClickListener clickListener) {
            this.moreListener = clickListener;
            return this;
        }

        public MeetLayoutBuilder starButtonListener(View.OnClickListener clickListener) {
            this.starListener = clickListener;
            return this;
        }

        public ConstraintLayout build() {
            ConstraintLayout layout = (ConstraintLayout) LayoutInflater.from(context).inflate(R.layout.meet_layout, null);
            TextView name = layout.findViewById(R.id.meet_name);
            TextView location = layout.findViewById(R.id.meet_location);
            TextView time = layout.findViewById(R.id.meet_time);
            Button starButton = layout.findViewById(R.id.star_button);
            MaterialCardView cardView = layout.findViewById(R.id.card_view);
            if (isFavourite) {
                cardView.setStrokeWidth(4);
                starButton.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_star_orange_button));
            } else {
                cardView.setStrokeWidth(0);
                starButton.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_star_gray_button));
            }

            starButton.setOnClickListener(starListener);
            Button moreButton = layout.findViewById(R.id.more_button);
            moreButton.setOnClickListener(moreListener);

            name.setText(this.name);
            location.setText(this.place);
            time.setText(this.time);
            return layout;
        }
    }

    public static MeetLayoutBuilder builder(Context context) {
        return new MeetLayoutBuilder(context);
    }
}
